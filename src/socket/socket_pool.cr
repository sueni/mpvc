struct SocketPool
  def initialize(@argv : Array(String))
  end

  def socket_files
    return selected_socket_files.files if @argv.delete "-ALL"
    selected_socket_files.first 1
  end

  private def selected_socket_files
    SocketFiles.new.tap do |sf|
      sf << Type::Top if @argv.delete "-Top"
      sf << Type::Norm if @argv.delete "-Norm"
      sf << Type::Low if @argv.delete "-Low"
      sf.concat Type.values if sf.empty?
    end
  end
end
