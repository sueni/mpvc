enum Type
  Top
  Norm
  Low

  def glob
    "#{SocketFiles::PREFIX}#{self}*"
  end
end
