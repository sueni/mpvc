require "json"

struct API
  @socket : UNIXSocket

  def initialize(socket_file : SocketFile)
    @socket = socket_file.to_socket
  end

  def action(args : Array(String))
    @socket.puts args.join(' ')
  end

  def get_property_string(property)
    unwrap(query(command("get_property", property))).as_s
  end

  private def command(command, property)
    {"command": ["#{command}", "#{property}"]}.to_json
  end

  private def query(command)
    @socket.puts command
    @socket.gets.not_nil!
  end

  private def unwrap(response)
    JSON.parse(response)["data"].not_nil!
  end
end
