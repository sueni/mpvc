struct SocketFiles
  PREFIX = "/tmp/mpv_socket_"

  def initialize
    @types = [] of Type
  end

  def files
    Array(SocketFile).new.tap do |sf|
      @types.each do |type|
        sf.concat(Dir[type.glob]
          .sort_by { |f| File.info(f).modification_time }
          .reverse
          .map { |f| SocketFile.new f, type })
      end
    end
  end

  delegate :empty?, :<<, :concat, to: @types
  delegate :first, to: files
end
