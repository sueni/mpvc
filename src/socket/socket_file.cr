require "socket"

struct SocketFile
  getter type : Type

  def initialize(@path : String, @type)
  end

  def to_socket
    UNIXSocket.new @path
  end

  def modification_time
    File.info(@path).modification_time
  end

  def type
    "some type"
  end
end
