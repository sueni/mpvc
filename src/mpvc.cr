require "option_parser"
require "./*"

socket_files = SocketPool.new(ARGV).socket_files

OptionParser.parse! do |parser|
  parser.on("-h", "--help", "Show this help") { puts parser }

  parser.on("--action", "Pass the command to socket intact") do
    socket_files.each do |socket_file|
      API.new(socket_file).action ARGV
    end
  end

  parser.on("--pause", "Pause") do
    socket_files.each do |socket_file|
      API.new(socket_file).action %w[set pause yes]
    end
  end

  parser.on("--toggle-pause", "Toggle pause") do
    if socket_files.any?
      socket_files.each do |socket_file|
        API.new(socket_file).action %w[cycle pause]
      end
    else
      Process.run("mpv-audio", {"/mnt/hdd/torrent/music"})
    end
  end

  parser.on("--volume-up", "Increase volume") do
    socket_files.each do |socket_file|
      API.new(socket_file).action %w[add volume 10]
    end
  end

  parser.on("--volume-down", "Decrease volume") do
    socket_files.each do |socket_file|
      API.new(socket_file).action %w[add volume -10]
    end
  end

  parser.on("--next-chapter", "Next chapter") do
    socket_files.each do |socket_file|
      API.new(socket_file).tap do |api|
        api.action %w[add chapter 1]
        api.action %w[show-progress]
      end
    end
  end

  parser.on("--prev-chapter", "Prev chapter") do
    socket_files.each do |socket_file|
      API.new(socket_file).tap do |api|
        api.action %w[add chapter -1]
        api.action %w[show-progress]
      end
    end
  end

  parser.on("--current-file", "Display path of the current file") do
    socket_files.each do |socket_file|
      puts API.new(socket_file).get_property_string("path")
    end
  end

  parser.invalid_option do |flag|
    STDERR.puts "ERROR: #{flag} is not a valid option"
    STDERR.puts parser
    exit 1
  end
end
